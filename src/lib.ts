let CustomAbortController;
let httpRequest;
let httpsRequest;
let parse;
import http from "http";
import https from "https";
import url from "url";
import __AbortController from "abort-controller"
const hasAbort = typeof AbortController !== "undefined"
if (hasAbort) {
    CustomAbortController = globalThis.AbortController
}
if (typeof window !== 'undefined') {
    // window
} else if (typeof require !== "undefined") {
    // CommonJS
    httpRequest = require("http").request;
    httpsRequest = require("https").request;
    parse = require("url").parse;
    CustomAbortController = require('abort-controller')
} else if (typeof globalThis === "object") {
    // ESM
    httpRequest = http.request;
    httpsRequest = https.request;
    parse = url.parse;
    CustomAbortController = __AbortController
} else {
    CustomAbortController = () => {
        throw new Error('AbortController is not defined');
    };
}

export { CustomAbortController, httpRequest, httpsRequest, parse };
