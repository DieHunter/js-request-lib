import typescript from "@rollup/plugin-typescript";
import json from "@rollup/plugin-json";
import { readFileSync } from "fs";
const packageJson = JSON.parse(readFileSync("./package.json", "utf8"));
const pkgName = packageJson.umdModuleName;
export default {
  input: "src/index.ts",
  output: [
    {
      file: "dist/esm/index.js",
      format: "esm",
    },
    {
      file: "dist/cjs/index.js",
      format: "cjs",
    },
    {
      file: "dist/umd/index.js",
      format: "umd",
      name: pkgName,
      globals: {
        "utils-lib-js": "UtilsLib",
      },
      external: ["utils-lib-js"],
    },
  ],
  plugins: [
    typescript({
      tsconfig: "./tsconfig.json",
    }),
    json(),
  ],
};
