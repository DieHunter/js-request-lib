# js-request

#### Introduction

The browser and node based on the JS implementation share the request module
Code redundancy, so isolated from https://gitee.com/DieHunter/utils-lib-js

#### Blog introduction

https://hunter1024.blog.csdn.net/article/details/126719561

#### Usage instructions

1. Use pnpm i utils-lib-js to install toolkit dependencies (choose one of two)
2. Install request dependencies using pnpm i js-request-lib (one of two options)
3. Use pnpm debug for source code debugging
4. Introduce the js-request-lib module into the code to create a request instance

```javascript
import { Request } from ".. /dist/esm/index.js";
const request = new Request("https://www.xxx.com");
```

5. Make a simple request using the GET method:

```javascript
request
.GET("/users", { page: 1, limit: 10 })
.then((response) => {
console.log("GET request successful ", response);
})
.catch((error) => {
console.error("GET request failed ", error);
});
```

6. Initiate a POST request

```javascript
const requestBody = { username: "hunter", password: "secret" };

request
.POST("/login", null, requestBody)
.then((response) => {
console.log("POST request successful ", response);
})
.catch((error) => {
console.error("POST request failed ", error);
});
```

7. In addition to GET and POST, the utility class also supports other request methods such as PUT, DELETE, OPTIONS, HEAD, and PATCH. It is used in a similar way, just call the corresponding method:

```javascript
// Initiate a PUT request
request
.PUT("/users/1", null, { name: "hunter" })
.then((response) => {
console.log("PUT request successful ", response);
})
.catch((error) => {
console.error("PUT request failed ", error);
});
```

8. Interceptors, through which you can add additional headers to the request and process the response data

```javascript
// Add a request interceptor
request.use("request", (config) => {
// Execute logic before the request is sent
console.log(" Request blocker - before request is sent ", config);
return config;
});

// Add a response interceptor
request.use("response", (response) => {
// Execute logic after response processing
console.log(" response interceptor - response processed ", response);
return response;
});
// Add an error blocker
request.use("error", (err) => {
// Execute logic when an error occurs
console.log(" Error - after processing ", err);
return err;
});
```

9. Set additional configurations when creating a request instance, such as setting timeout times, custom requests, etc. :

```javascript
const customRequest = new Request("https://api.example.com", {
timeout: 5000, // Set the timeout period to 5 seconds
headers: {
Authorization: "Bearer YOUR_ACCESS_TOKEN", // Sets custom request headers
"Content-Type": "application/json",
},
});

customRequest
.GET("/data")
.then((response) => {
console.log(" Request successful ", response);
})
.catch((error) => {
console.error(" Request failed ", error);
});
```

#### Contribute

1. Fork the local warehouse
2. Star Warehouse
3. Make suggestions
4. Create a Pull Request

